/**
 * Crear funciones en typescript
 * 
*/

interface myCharacter{
    name: string;
    hp: number;
    showHp: () => void
}

function addNumbers(a: number, b:number){

    return a+b;
}

function multiply(firstNumber: number, secondNumber?:number, base:number = 15){
    return firstNumber * base;
}
console.log(multiply(3, 3));


const healCharacter = (character: myCharacter, amount: number) => {
    character.hp += amount;
}
const strider: myCharacter = {
    name: 'carla',
    hp: 100,
    showHp(){
        console.log(`Puntos de vida ${this.hp}`);
        
    }
}

healCharacter(strider, 50);
strider.showHp();
