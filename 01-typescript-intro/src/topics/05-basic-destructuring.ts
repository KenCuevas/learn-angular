interface AudioPlayer{
    audioVolume: number;
    songDuration: number;
    song: string;
    details: Details;
}

interface Details {
    author: string;
    year: number;
}


const audioPlayer : AudioPlayer = {
    audioVolume: 65,
    songDuration: 36,
    song: "Highway to hell",
    details: {
        author: "AC/DC",
        year: 1979
    }
}

//Desestructuracion
const {song:anotherSong, songDuration:duration, details:{author}} = audioPlayer;

console.log(`Song: ${anotherSong}`);
console.log(`Duration: ${duration}`);
console.log(`Author: ${author}`);
