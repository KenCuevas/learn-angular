export interface Product{
    name: string;
    description: string;
    price: number;
}

const phone: Product = {
    name: "Iphone X",
    description: "125 GB de memoria",
    price: 250
}
const tablet: Product = {
    name: "Ipad Air",
    description: "125 GB de memoria",
    price: 300
}

const shoppingCart = [phone, tablet];

const tax = 0.18;

interface TaxCalculationOptions{
    tax: number;
    products: Product[];
}

function taxCalculator(options: TaxCalculationOptions): number[]{
    let total = 0;

    options.products.forEach( product => {
        total += product.price
    })

    return [total, total * options.tax]
}