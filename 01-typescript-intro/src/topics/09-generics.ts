function whatsMyType<T>(argument:T): T{
    return argument;
}

let amINumber = whatsMyType<number>(125);

console.log(amINumber);
