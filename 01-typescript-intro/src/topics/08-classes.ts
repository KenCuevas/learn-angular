export class Person{
    public name: string;
    public lastname: string;
    public age?: number;

    constructor(name:string, lastname:string, age?: number){
        this.name = name,
        this.lastname = lastname
        this.age = age
    }
}

class Hero{
    public power: string;
    public person: Person;

    constructor(power: string, person:Person){
        this.power = power
        this.person = person;
    }
    
}
const person = new Person('Tony', 'Stark');
const hulk = new Person('Dr. Robert', 'Bruce Banner', 32);

const ironMan = new Hero('Anthony Edward', 'Stark', person);
console.log(hulk);
console.log(ironMan);
