let skills: string[] = ['Bash', 'Counter', 'Healing'];

interface Character{
    name: string;
    hp: number;
    skills: string[];
    homeTown?: string;
}

const person: Character = {
    name: 'Kenny',
    hp: 100,
    skills: ['Through Fire'],
    homeTown: 'Pantojas'
};

console.table(person)